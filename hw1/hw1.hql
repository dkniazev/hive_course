--1 number of flights per carrier 
SELECT COUNT(flightnum) as flight_per_carrier, uniquecarrier FROM hw1 GROUP BY uniquecarrier;

--2 total number of flights served in Jun 2007 by NYC 
SELECT COUNT(flightnum) as flight_per_carrier FROM hw1 
LEFT JOIN airports AS ap_o ON hw1.origin=ap_o.iata
LEFT JOIN airports AS ap_d ON hw1.dest=ap_d.iata
WHERE hw1.month = 6 AND (ap_o.city = 'New York' OR ap_d.city = 'New York');

--3 most busy airports
SELECT COALESCE(orig.o, dest.d) as airport, COALESCE(dest.sum_d, 0) + COALESCE(orig.sum_o, 0) as sum FROM 
(SELECT hw1.origin as o, COUNT(flightnum) as sum_o FROM hw1
    JOIN airports AS ap_o ON hw1.origin=ap_o.iata
    WHERE hw1.year = 2007 AND hw1.month >= 6 AND hw1.month < 9 AND ap_o.country = 'USA'
    GROUP BY hw1.origin) as orig
    FULL JOIN (SELECT hw1.dest as d, COUNT(flightnum) as sum_d FROM hw1
      JOIN airports AS ap_d ON hw1.dest=ap_d.iata
      WHERE hw1.year = 2007 AND hw1.month >= 6 AND hw1.month < 9 AND ap_d.country = 'USA'
      GROUP BY hw1.dest) as dest ON dest.d = orig.o
ORDER BY sum DESC
LIMIT 5;

--4 carrier served biggest number of flight
SELECT COUNT(flightnum) as flight_per_carrier, uniquecarrier FROM hw1 
GROUP BY uniquecarrier
ORDER BY flight_per_carrier DESC
LIMIT 1;
