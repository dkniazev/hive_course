--1 carriers who cancelled more than 1 flights with record all departure cities where cancellation happened.
SELECT 
	uniquecarrier, 	
	COUNT(flightnum) as cancelled_num, 
	concat_ws(', ', collect_set(ap_o.city)) as cityes 
FROM hw1
JOIN airports AS ap_o ON hw1.origin=ap_o.iata
WHERE cancelled = true 
GROUP BY uniquecarrier
HAVING COUNT(flightnum) > 1
ORDER BY cancelled_num DESC;

--Q How many MR jobs where instanced for this query?
--A Started 2 MR job "Map 1" and "Map 4" with 6 and 1 mappers respectively;

