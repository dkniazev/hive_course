package com.epam.uaparser;

import net.sf.uadetector.*;
import net.sf.uadetector.service.UADetectorServiceFactory;
import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDTF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitrii_Kniazev
 * @since 12/01/2016
 */
@Description(name = "ua_parser", value = "_FUNC_(expr) - parse user-agent " +
        "string to following columns:\n" +
        "UA type\n" +
        "UA family\n" +
        "OS name\n" +
        "Device type")
public final class UaParserUdtf extends GenericUDTF {
    private static final UserAgentStringParser USER_AGENT_PARSER =
            UADetectorServiceFactory.getResourceModuleParser();

    private transient PrimitiveObjectInspector stringOI = null;

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("deprecation")
    @Override
    public StructObjectInspector initialize(final ObjectInspector[] args)
            throws UDFArgumentException {
        if (args.length != 1) {
            throw new UDFArgumentException("UaParserUdtf() takes exactly one argument");
        }
        if (args[0].getCategory() != ObjectInspector.Category.PRIMITIVE
                && ((PrimitiveObjectInspector) args[0]).getPrimitiveCategory()
                != PrimitiveObjectInspector.PrimitiveCategory.STRING) {
            throw new UDFArgumentException("UaParserUdtf() takes a string as a parameter");
        }
        // input inspectors
        stringOI = (PrimitiveObjectInspector) args[0];

        // output
        final List<String> fieldNames = new ArrayList<>();
        fieldNames.add("uatype");
        fieldNames.add("uafamily");
        fieldNames.add("osname");
        fieldNames.add("device");

        final List<ObjectInspector> fieldOIs = new ArrayList<>();
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);

        return ObjectInspectorFactory.getStandardStructObjectInspector(fieldNames, fieldOIs);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void process(final Object[] args) throws HiveException {
        final String uaString = stringOI.getPrimitiveJavaObject(args[0]).toString();
        forward(parseUa(uaString));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() throws HiveException {
    }

    private Object[] parseUa(final String uaString) {
        final ReadableUserAgent agent = USER_AGENT_PARSER.parse(uaString);

        final UserAgentType type = agent.getType();
        final UserAgentFamily family = agent.getFamily();
        final OperatingSystem os
                = agent.getOperatingSystem();
        final ReadableDeviceCategory device
                = agent.getDeviceCategory();

        return new Object[]{
                type.getName(),
                family.getName(),
                os.getName(),
                device.getName()};
    }
}
