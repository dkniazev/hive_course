--import custom udf function
add jar /udf/uaparser-1.0-SNAPSHOT.jar;
create temporary function ua_parser as 'com.epam.uaparser.UaParserUdtf';
--create table for transitional data
CREATE TEMPORARY TABLE IF NOT EXISTS imp_tmp (
    city INT, 
    uatype STRING,
    uafamily STRING,
    osname STRING,
    device STRING)
COMMENT 'IMP TEMPORARY TABLE'
ROW FORMAT DELIMITED;
--fill the table with data
INSERT OVERWRITE TABLE imp_tmp SELECT  
		t.city, 
        adTable.uatype,  
        adTable.uafamily,  
        adTable.osname,  
        adTable.device  
FROM imp AS t
    LATERAL VIEW ua_parser(user_agent) adTable AS uatype, uafamily, osname, device;
--find most popular device, browser, OS for each city
SELECT 
    COALESCE(ct.city_name, 'N/A'), 
	city.id
    device.device,
    browser.uafamily,
    os.osname
--find all unique cities     
FROM (
    SELECT t.city as id FROM imp_tmp AS t
    GROUP BY t.city
) as city
--join cities
LEFT JOIN cities_table as ct on ct.city_id = city.id
--join most popular devices
JOIN(
    SELECT dvc.city_d, dvc.device FROM 
    (SELECT 
        dt.city_d, 
        dt.device,
    	ROW_NUMBER() OVER(PARTITION BY dt.city_d ORDER BY dt.count DESC) AS row_num
    FROM (SELECT t.city as city_d, t.device ,COUNT(device) AS count FROM imp_tmp AS t 
            GROUP BY t.city, t.device) AS dt ) AS dvc
WHERE dvc.row_num = 1    
) as device on device.city_d=city.id
--join most popular browsers
JOIN (
    SELECT brw.city_b, brw.uafamily FROM 
    (SELECT 
        bt.city_b, 
        bt.uafamily,
    	ROW_NUMBER() OVER(PARTITION BY bt.city_b ORDER BY bt.count DESC) AS row_num
    FROM (SELECT t.city as city_b, t.uafamily ,COUNT(uafamily) AS count FROM imp_tmp AS t 
            WHERE t.uatype = 'Browser'
            GROUP BY t.city, t.uafamily) AS bt ) AS brw
WHERE brw.row_num = 1
) as browser on browser.city_b=city.id
--join most popular OS
JOIN (
    SELECT ost.city_o, ost.osname FROM 
    (SELECT 
        ot.city_o, 
        ot.osname,
    	ROW_NUMBER() OVER(PARTITION BY ot.city_o ORDER BY ot.count DESC) AS row_num
    FROM (SELECT t.city as city_o, t.osname ,COUNT(osname) AS count FROM imp_tmp AS t 
            GROUP BY t.city, t.osname) AS ot ) AS ost
WHERE ost.row_num = 1
) as os on os.city_o=city.id
--sort by sity
SORT BY city.id;
