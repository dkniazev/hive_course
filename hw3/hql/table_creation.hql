-- create table using temporary table
CREATE TABLE IF NOT EXISTS imp(
        bid_id STRING,
        timestamp_f STRING,
        log_type STRING,
        ipinyou_id STRING,
        user_agent 	STRING,
        ip STRING,
        region INT,
        city INT,
        ad_exchange STRING,
        domain STRING,
        url STRING,
        anonymous_url_id STRING,
        ad_slot_id STRING,
        ad_slot_width STRING,
        ad_slot_height STRING,
        ad_slot_visibility STRING,
        ad_slot_format STRING,
        ad_slot_floor_price STRING,
        creative_id STRING,
        bidding_price STRING,
        paying_price STRING,
        key_page_url STRING,
        advertiser_id STRING,
        user_tags STRING)
    COMMENT 'Impression, Click, and Conversion Log'
    ROW FORMAT DELIMITED
    FIELDS TERMINATED BY '\t'
    STORED AS ORC;

CREATE TEMPORARY TABLE IF NOT EXISTS tmp_table(
        bid_id STRING,
        timestamp_f STRING,
        log_type STRING,
        ipinyou_id STRING,
        user_agent 	STRING,
        ip STRING,
        region INT,
        city INT,
        ad_exchange STRING,
        domain STRING,
        url STRING,
        anonymous_url_id STRING,
        ad_slot_id STRING,
        ad_slot_width STRING,
        ad_slot_height STRING,
        ad_slot_visibility STRING,
        ad_slot_format STRING,
        ad_slot_floor_price STRING,
        creative_id STRING,
        bidding_price STRING,
        paying_price STRING,
        key_page_url STRING,
        advertiser_id STRING,
        user_tags STRING)
    COMMENT 'tmp'
    ROW FORMAT DELIMITED
    FIELDS TERMINATED BY '\t'
    STORED AS TEXTFILE
    location '/hwh3/';

INSERT OVERWRITE TABLE imp SELECT * FROM tmp_table;

CREATE EXTERNAL TABLE IF NOT EXISTS cities_table(
        city_id STRING,
        city_ STRING)
    COMMENT 'cities'
    ROW FORMAT DELIMITED
    FIELDS TERMINATED BY '\t'
    STORED AS TEXTFILE
    location '/cities/';
