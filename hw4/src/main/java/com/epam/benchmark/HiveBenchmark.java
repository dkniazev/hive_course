package com.epam.benchmark;

import org.apache.log4j.Logger;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.sql.*;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
public class HiveBenchmark {

    private static final Logger LOG = Logger.getLogger(HiveBenchmark.class);
    private static final String DRIVER_NAME = "org.apache.hive.jdbc.HiveDriver";
    private static final String USER = "root";
    private static final String PASSWORD = "hadoop";
    private static final String URL = "jdbc:hive2://localhost:10000";

    private static final String ENABLE_MR_ENGINE = "SET hive.execution.engine=mr";
    private static final String ENABLE_TEZ_ENGINE = "SET hive.execution.engine=tez";
    private static final String ENABLE_VECTOR = "SET hive.vectorized.execution.enabled = true";
    private static final String ENABLE_VECTOR_REDUCER = "SET hive.vectorized.execution.reduce.enabled = true";
    private static final String DISABLE_VECTOR = "SET hive.vectorized.execution.enabled = false";
    private static final String DISABLE_VECTOR_REDUCER = "SET hive.vectorized.execution.reduce.enabled = false";

    private Connection connection;

    @Setup
    public void prepare() throws SQLException {
        try {
            Class.forName(DRIVER_NAME);
        } catch (ClassNotFoundException e) {
            LOG.error("Hive driver not found");
            System.exit(1);
        }
        LOG.debug("Hive driver found");
        this.connection = DriverManager.getConnection(URL, USER, PASSWORD);
        LOG.warn("Connected to " + URL + " " + USER + " " + PASSWORD);
    }

    @TearDown
    public void check() throws SQLException {
        connection.close();
        LOG.warn("Connection closed");
    }


    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
    public void testHw1WithIndexes() throws SQLException {
        final Statement statement = getStatement();
        statement.execute(ENABLE_TEZ_ENGINE);
        statement.execute(DISABLE_VECTOR);
        statement.execute(DISABLE_VECTOR_REDUCER);
        final ResultSet res = statement.executeQuery(getHql("hw1"));
        res.next();
        assert assertResult(res.getString(1));
        statement.close();
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
    public void testHw4V1() throws SQLException {
        final Statement statement = getStatement();
        statement.execute(ENABLE_TEZ_ENGINE);
        statement.execute(DISABLE_VECTOR);
        statement.execute(DISABLE_VECTOR_REDUCER);
        final ResultSet res = statement.executeQuery(getHql("hw4v1"));
        res.next();
        assert assertResult(res.getString(1));
        statement.close();
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
    public void testHw4V2() throws SQLException {
        final Statement statement = getStatement();
        statement.execute(ENABLE_TEZ_ENGINE);
        statement.execute(DISABLE_VECTOR);
        statement.execute(DISABLE_VECTOR_REDUCER);
        final ResultSet res = statement.executeQuery(getHql("hw4v2"));
        res.next();
        assert assertResult(res.getString(1));
        statement.close();
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
    public void testHw4V3() throws SQLException {
        final Statement statement = getStatement();
        statement.execute(ENABLE_TEZ_ENGINE);
        statement.execute(DISABLE_VECTOR);
        statement.execute(DISABLE_VECTOR_REDUCER);
        final ResultSet res = statement.executeQuery(getHql("hw4v3"));
        res.next();
        assert assertResult(res.getString(1));
        statement.close();
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
    public void testHw4V4() throws SQLException {
        final Statement statement = getStatement();
        statement.execute(ENABLE_TEZ_ENGINE);
        statement.execute(DISABLE_VECTOR);
        statement.execute(DISABLE_VECTOR_REDUCER);
        final ResultSet res = statement.executeQuery(getHql("hw4v4"));
        res.next();
        assert assertResult(res.getString(1));
        statement.close();
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
    public void testHw1VectorsMr() throws SQLException {
        final Statement statement = getStatement();
        LOG.debug("Statement created");
        statement.execute(ENABLE_MR_ENGINE);
        statement.execute(ENABLE_VECTOR);
        statement.execute(ENABLE_VECTOR_REDUCER);
        final ResultSet res = statement.executeQuery(getHql("hw1"));
        res.next();
        assert assertResult(res.getString(1));
        statement.close();
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
    public void testHw1VectorsTez() throws SQLException {
        final Statement statement = getStatement();
        LOG.debug("Statement created");
        statement.execute(ENABLE_TEZ_ENGINE);
        statement.execute(ENABLE_VECTOR);
        statement.execute(ENABLE_VECTOR_REDUCER);
        final ResultSet res = statement.executeQuery(getHql("hw1"));
        res.next();
        assert assertResult(res.getString(1));
        statement.close();
    }

    private Statement getStatement() throws SQLException {
        if (this.connection == null) {
            LOG.error("No connection");
            System.exit(2);
        }
        return connection.createStatement();
    }

    public static void main(String[] args) throws RunnerException {
        final Options opt = new OptionsBuilder()
                .include(HiveBenchmark.class.getSimpleName())
                .warmupIterations(0)
                .measurementIterations(200)
                .forks(1)
                .threads(1)
                .jvmArgs("-ea")
                .build();
        new Runner(opt).run();
    }

    private String getHql(final String tableName) {
        return "SELECT COUNT(flightnum) as flight_per_carrier FROM " + tableName + " AS t" +
                " LEFT JOIN airports AS ap_o ON t.origin=ap_o.iata" +
                " LEFT JOIN airports AS ap_d ON t.dest=ap_d.iata" +
                " WHERE t.month = 6 AND (ap_o.city = 'New York' OR ap_d.city = 'New York')";
    }

    private boolean assertResult(String result) {
        return "41280".equals(result);
    }
}
