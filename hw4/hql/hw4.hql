--create table with partitioning/bucketing ver.1
CREATE TABLE IF NOT EXISTS hw4v1(
    year INT,
    dayofmonth INT,
    dayofweek INT,
    deptime STRING,
    crsdeptime STRING,
    arrtime STRING,
    crsarrtime STRING,
    uniquecarrier STRING,
    flightnum STRING,
    tailnum STRING,
    actualelapsedtime STRING,
    crselapsedtime STRING,
    airtime STRING,
    arrdelay STRING,
    depdelay STRING,
    origin STRING,
    dest STRING,
    distance STRING,
    taxiin STRING,
    taxiout STRING,
    cancelled BOOLEAN,
    cancellationcode STRING,
    diverted STRING,
    carrierdelay STRING,
    weatherdelay STRING,
    nasdelay STRING,
    securitydelay STRING,
    lateaircraftdelay STRING)
COMMENT 'TABLE WITH PARTITION 1'
PARTITIONED BY (month INT)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS ORC;
--fill table hw4v1
SET hive.exec.dynamic.partition = true;
SET hive.exec.dynamic.partition.mode = nonstrict;

INSERT OVERWRITE TABLE hw4v1
PARTITION (month)
SELECT 
    year,
    dayofmonth,
    dayofweek,
    deptime,
    crsdeptime,
    arrtime,
    crsarrtime,
    uniquecarrier,
    flightnum,
    tailnum,
    actualelapsedtime,
    crselapsedtime,
    airtime,
    arrdelay,
    depdelay,
    origin,
    dest,
    distance,
    taxiin,
    taxiout,
    cancelled,
    cancellationcode,
    diverted,
    carrierdelay,
    weatherdelay,
    nasdelay,
    securitydelay,
    lateaircraftdelay,
    month FROM hw1;

--create table with partitioning/bucketing ver.2
CREATE TABLE IF NOT EXISTS hw4v2(
    year INT,
    dayofmonth INT,
    dayofweek INT,
    deptime STRING,
    crsdeptime STRING,
    arrtime STRING,
    crsarrtime STRING,
    uniquecarrier STRING,
    flightnum STRING,
    tailnum STRING,
    actualelapsedtime STRING,
    crselapsedtime STRING,
    airtime STRING,
    arrdelay STRING,
    depdelay STRING,
    origin STRING,
    dest STRING,
    distance STRING,
    taxiin STRING,
    taxiout STRING,
    cancelled BOOLEAN,
    cancellationcode STRING,
    diverted STRING,
    carrierdelay STRING,
    weatherdelay STRING,
    nasdelay STRING,
    securitydelay STRING,
    lateaircraftdelay STRING)
COMMENT 'TABLE WITH PARTITION 2'
PARTITIONED BY (month INT)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS PARQUET;
--fill table hw4v2
SET hive.exec.dynamic.partition = true;
SET hive.exec.dynamic.partition.mode = nonstrict;

INSERT OVERWRITE TABLE hw4v2
PARTITION (month)
SELECT 
    year,
    dayofmonth,
    dayofweek,
    deptime,
    crsdeptime,
    arrtime,
    crsarrtime,
    uniquecarrier,
    flightnum,
    tailnum,
    actualelapsedtime,
    crselapsedtime,
    airtime,
    arrdelay,
    depdelay,
    origin,
    dest,
    distance,
    taxiin,
    taxiout,
    cancelled,
    cancellationcode,
    diverted,
    carrierdelay,
    weatherdelay,
    nasdelay,
    securitydelay,
    lateaircraftdelay,
    month FROM hw1;

--create table with partitioning/bucketing ver.3
CREATE TABLE IF NOT EXISTS hw4v3(
    year INT,
    --month INT,
    dayofmonth INT,
    dayofweek INT,
    deptime STRING,
    crsdeptime STRING,
    arrtime STRING,
    crsarrtime STRING,
    uniquecarrier STRING,
    flightnum STRING,
    tailnum STRING,
    actualelapsedtime STRING,
    crselapsedtime STRING,
    airtime STRING,
    arrdelay STRING,
    depdelay STRING,
    origin STRING,
    dest STRING,
    distance STRING,
    taxiin STRING,
    taxiout STRING,
    cancelled BOOLEAN,
    cancellationcode STRING,
    diverted STRING,
    carrierdelay STRING,
    weatherdelay STRING,
    nasdelay STRING,
    securitydelay STRING,
    lateaircraftdelay STRING)
COMMENT 'TABLE WITH PARTITION 3'
PARTITIONED BY (month INT)
CLUSTERED BY (origin, dest) INTO 32 BUCKETS
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS ORC;
--fill table hw4v3
SET hive.exec.dynamic.partition = true;
SET hive.exec.dynamic.partition.mode = nonstrict;
SET hive.enforce.bucketing = true;

INSERT OVERWRITE TABLE hw4v3
PARTITION (month)
SELECT 
    year,
    dayofmonth,
    dayofweek,
    deptime,
    crsdeptime,
    arrtime,
    crsarrtime,
    uniquecarrier,
    flightnum,
    tailnum,
    actualelapsedtime,
    crselapsedtime,
    airtime,
    arrdelay,
    depdelay,
    origin,
    dest,
    distance,
    taxiin,
    taxiout,
    cancelled,
    cancellationcode,
    diverted,
    carrierdelay,
    weatherdelay,
    nasdelay,
    securitydelay,
    lateaircraftdelay,
    month FROM hw1;

--create table with partitioning/bucketing ver.4
CREATE TABLE IF NOT EXISTS hw4v4(
    year INT,
    --month INT,
    dayofmonth INT,
    dayofweek INT,
    deptime STRING,
    crsdeptime STRING,
    arrtime STRING,
    crsarrtime STRING,
    uniquecarrier STRING,
    flightnum STRING,
    tailnum STRING,
    actualelapsedtime STRING,
    crselapsedtime STRING,
    airtime STRING,
    arrdelay STRING,
    depdelay STRING,
    origin STRING,
    dest STRING,
    distance STRING,
    taxiin STRING,
    taxiout STRING,
    cancelled BOOLEAN,
    cancellationcode STRING,
    diverted STRING,
    carrierdelay STRING,
    weatherdelay STRING,
    nasdelay STRING,
    securitydelay STRING,
    lateaircraftdelay STRING)
COMMENT 'TABLE WITH PARTITION 4'
PARTITIONED BY (month INT)
CLUSTERED BY (origin, dest) SORTED BY (uniquecarrier) INTO 32 BUCKETS
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS ORC;

SET hive.exec.dynamic.partition = true;
SET hive.exec.dynamic.partition.mode = nonstrict;
SET hive.enforce.bucketing = true;

INSERT OVERWRITE TABLE hw4v4
PARTITION (month)
SELECT 
    year,
    dayofmonth,
    dayofweek,
    deptime,
    crsdeptime,
    arrtime,
    crsarrtime,
    uniquecarrier,
    flightnum,
    tailnum,
    actualelapsedtime,
    crselapsedtime,
    airtime,
    arrdelay,
    depdelay,
    origin,
    dest,
    distance,
    taxiin,
    taxiout,
    cancelled,
    cancellationcode,
    diverted,
    carrierdelay,
    weatherdelay,
    nasdelay,
    securitydelay,
    lateaircraftdelay,
    month FROM hw1;

--index creation
CREATE INDEX index_carrier ON hw1(uniquecarrier)
AS 'org.apache.hadoop.hive.ql.index.compact.CompactIndexHandler'
WITH DEFERRED REBUILD;

--index rebuild
ALTER INDEX index_carrier ON hw1 REBUILD;

--enable vectorization
set hive.vectorized.execution.enabled = true;
set hive.vectorized.execution.reduce.enabled = true;




























